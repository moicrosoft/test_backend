from ..serializers.CreateItemSerializer import CreateItemSerializer
from .CreateAPIView import CreateAPIView


class CreateItem(CreateAPIView):
    serializer_class = CreateItemSerializer
    name = "Artículo"