from ..models.Order import Order
from ..serializers.DetailOrderSerializer import DetailOrderSerializer
from rest_framework import generics


class RetrieveOrder(generics.RetrieveDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = DetailOrderSerializer
    lookup_field = 'id'
    name = "Pedido"