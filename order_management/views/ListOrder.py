from ..models.Order import Order
from ..serializers.DetailOrderSerializer import DetailOrderSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics


class ListOrder(generics.ListAPIView):
    queryset = Order.objects.all()
    serializer_class = DetailOrderSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id', 'creation_date', 'price_no_tax', 'tax_percentage', 'price_with_tax', 'currency']
    name = "Pedidos"