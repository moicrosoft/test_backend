from django.shortcuts import redirect
from django.contrib import messages
from rest_framework import generics


class CreateAPIView(generics.CreateAPIView):
    redirect_url = "/"

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            self.create(request, *args, **kwargs)
            messages.success(request._request, 'El %s se guardó satisfactoriamente'%self.name.lower())
            return redirect(self.redirect_url)
        return super().post(request, *args, **args)
