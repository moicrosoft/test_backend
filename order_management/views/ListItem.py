from ..models.Item import Item
from ..serializers.DetailItemSerializer import DetailItemSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics


class ListItem(generics.ListAPIView):
    serializer_class = DetailItemSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['id', 'name', 'description', 'units', 'price_no_tax']
    name = "Pedidos"

    def get_queryset(self):
        return Item.objects.filter(order__id=self.kwargs.get("order"))