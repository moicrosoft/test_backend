from ..models.Item import Item
from ..serializers.DetailItemSerializer import DetailItemSerializer
from .RetrieveDestroyAPIView import RetrieveDestroyAPIView


class RetrieveItem(RetrieveDestroyAPIView):
    queryset = Item.objects.all()
    serializer_class = DetailItemSerializer
    lookup_field = 'id'
    name = "Artículo"