from ..serializers.CreateOrderSerializer import CreateOrderSerializer
from .CreateAPIView import CreateAPIView


class CreateOrder(CreateAPIView):
    serializer_class = CreateOrderSerializer
    name = "Pedido"