from django.shortcuts import redirect
from django.contrib import messages
from rest_framework import generics


class RetrieveDestroyAPIView(generics.RetrieveDestroyAPIView):
    redirect_url = "/"

    def get(self, request, *args, **kwargs):
        if request.GET.get("method", None)=="DELETE":
            self.destroy(request, *args, **kwargs)
            messages.success(request._request, 'El %s se eliminó satisfactoriamente'%self.name.lower())
            return redirect(self.redirect_url)
        return super().get(request, *args, **kwargs)