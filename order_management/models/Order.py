from django.db import models
from django.core.validators import MaxValueValidator


class Order(models.Model):

    CURRENCIES = [("COP","Peso colombiano (COP)"),("USD","Dolar estadounidense (USD)"),("EUR","Euro (EUR)")]
    id = models.AutoField(primary_key=True, verbose_name="Número de pedido")
    creation_date = models.DateTimeField(auto_now_add=True, null=False, verbose_name="Fecha de creación")
    price_no_tax = models.PositiveIntegerField(null=False, default=0, verbose_name="Precio sin impuestos")
    tax_percentage = models.PositiveSmallIntegerField(null=False, blank=False, validators=[MaxValueValidator(100)], verbose_name="Porcentaje de impuestos")
    price_with_tax = models.PositiveIntegerField(null=False, default=0, verbose_name="Precio con impuestos")
    currency = models.CharField(max_length=50, null=False, blank=False, choices=CURRENCIES, verbose_name="Moneda")

    def update(self, itemValue):
        self.price_no_tax += itemValue
        self.price_with_tax = self.price_no_tax + (self.price_no_tax * self.tax_percentage / 100)
        self.save()

    def __str__(self):
        return "Pedido #%s: (%s)"%(self.id, self.creation_date.strftime("%d/%m/%Y %I:%M:%S %p"))