from django.db import models
from django.db.models.deletion import CASCADE
from .Order import Order


class Item(models.Model):

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name="Nombre")
    description = models.TextField(null=True, blank=True, verbose_name="Descripcion")
    units = models.PositiveSmallIntegerField(null=False, blank=True, default=0, verbose_name="Unidades")
    price_no_tax = models.PositiveIntegerField(null=False, blank=False, verbose_name="Precio sin impuestos")
    order = models.ForeignKey(Order, on_delete=CASCADE, null=False, blank=False, verbose_name="Identificador de pedido")

    def save(self, *args, **kwargs):
        self.order.update(self.price_no_tax*self.units)
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.order.update(-self.price_no_tax*self.units)
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name
