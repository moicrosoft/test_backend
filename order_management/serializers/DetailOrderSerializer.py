from ..models.Order import Order
from ..models.Item import Item
from rest_framework import serializers


class DetailOrderSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()

    def get_items(self, value):
        return ", ".join(["%s. %s"%(i.id, i.name) for i in Item.objects.filter(order=value)])

    class Meta:
        model = Order
        fields = ['id', 'creation_date', 'price_no_tax', 'tax_percentage', 'price_with_tax', 'currency', 'items']
