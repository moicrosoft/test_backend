from ..models.Item import Item
from rest_framework import serializers


class CreateItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = ['name', 'description', 'units', 'price_no_tax', 'order']
