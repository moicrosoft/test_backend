from ..models.Order import Order
from rest_framework import serializers


class CreateOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['tax_percentage', 'currency']
