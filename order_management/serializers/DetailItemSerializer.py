from ..models.Item import Item
from rest_framework import serializers


class DetailItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Item
        fields = ['id', 'name', 'description', 'units', 'price_no_tax', 'order']
