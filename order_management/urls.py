from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^create-item/?', views.CreateItem.as_view()),
    re_path(r'^create-order/?', views.CreateOrder.as_view()),
    re_path(r'^list-order/?', views.ListOrder.as_view()),
    re_path(r'^list-item/order/(?P<order>\d+)', views.ListItem.as_view()),
    re_path(r'^retrieve-order/(?P<id>\d+)', views.RetrieveOrder.as_view()),
    re_path(r'^retrieve-item/(?P<id>\d+)', views.RetrieveItem.as_view()),
]
