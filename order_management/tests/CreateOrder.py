from rest_framework.test import RequestsClient

client = RequestsClient()
response = client.post('http://localhost:8000/create-order/', {'tax_percentage': 30, 'currency': 'COP'})
assert response.status_code == 200