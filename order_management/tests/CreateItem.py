from rest_framework.test import RequestsClient

client = RequestsClient()
response = client.post('http://localhost:8000/create-item/', {'name': 'nevera', 'description': 'nevera de 700L',
                                                              'units': 2, 'price_no_tax': 800000, 'order': 1
                                                              })
assert response.status_code == 200

