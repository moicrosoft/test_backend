from rest_framework.test import RequestsClient

client = RequestsClient()
response = client.delete('http://localhost:8000/retrieve-item/1')
assert response.status_code == 204