defaultAutoclose = '20000';

$(document).ready(function(){
    messageInput = $("#id_message");
    message = messageInput.attr("value");
    if(message!=undefined){
        tag = messageInput.attr("tag");
        messageDialog(message, tag, "");
    }
});

function success(message, title="Éxito"){
    messageDialog(message, 'success', title);
}

function info(message, title="Info"){
    messageDialog(message, 'info', title);
}

function error(message, title="Error"){
    messageDialog(message, 'danger', title);
}

function warning(message, title="Precaución"){
    messageDialog(message, 'warning', title);
}

function messageDialog(message, tag, title){
    title = title || "";
    type = tag=='success' ? 'green' : tag=='info' ? 'blue' : tag=='danger' ? 'red' : 'orange';
    icon = (tag=='success' || tag=='info') ? 'fas fa-check-circle' : 'fas fa-exclamation-triangle';
    return new Promise(function(resolve, reject) {
        $.confirm({
            title: title,
            content: message,
            theme: 'modern',
            type: type,
            icon: icon,
            buttons: {   
                accept: {
                    text: "Aceptar",
                    btnClass: 'btn-'+tag+' text-white',
                },
            }
        });
    });
}

function confirmDialog(title, message, func, buttonText){
    buttonText = buttonText || "Aceptar";
    return new Promise(function(resolve, reject) {
        $.confirm({
            title: title,
            content: message,
            theme: 'modern',
            type: 'orange',
            icon: 'fas fa-exclamation-circle',
            buttons: {
                accept: {
                    text: buttonText,
                    btnClass: 'btn-primary text-white',
                    action: function () {
                        func();
                    }
                },
                cancel: {
                    text: "Cancelar",
                }
            }
        });
    });
}
