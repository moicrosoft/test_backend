$(document).ready(function(){
    comp = ['input', 'textarea'];
    for(c in comp){
        $(comp[c]).addClass("form-control");
    }
    $(".navbar li.dropdown").hover(function(e){
        var dropdownMenu = $(this).children(".dropdown-menu");
        if(e.type=="mouseenter"){
            dropdownMenu.fadeIn(300);
        }
        else if(e.type=="mouseleave"){
            dropdownMenu.fadeOut(300);
        }
    });
});