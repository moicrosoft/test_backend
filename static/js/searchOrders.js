var modal, list;

$(document).ready(function(){
    modal = $("#modal-list");
    list = modal.find("div.modal-body ul.list-group");
});

function initModal(title){
    modal.find(".modal-title").text(title);
    list.empty();
}

function getDate(date){
    return $.datepicker.formatDate("dd/mm/yy", new Date(date));
}

function retrieveLinks(data, url){
    initModal("Pedidos");
    for(d in data){
        date = getDate(data[d].creation_date);
        list.append('<a class="list-group-item list-group-item-action" href="/'+url+data[d].id+'"> Pedido #'+data[d].id+' ('+date+')</a>');
    }
}

function orderLinks(data){
    retrieveLinks(data, "retrieve-order/");
}

function itemLinksByOrder(data){
    retrieveLinks(data, "list-item/order/");
}

function orderLinksAlt(data){
    initModal("Pedidos");
    for(d in data){
        url = "/list-item/order/"+data[d].id;
        date = getDate(data[d].creation_date);
        list.append('<button class="list-group-item list-group-item-action btn" onclick="search(itemLinksById,\''+url+'\')">Pedido #'+data[d].id+' ('+date+')</button>');
    }
}

function itemLinksById(data){
    modal.modal("hide");
    initModal("Artículos");
    for(d in data){
        list.append('<a class="list-group-item list-group-item-action" href="/retrieve-item/'+data[d].id+'"> Artículo #'+data[d].id+' - '+data[d].name+'</a>');
    }
}

function search(func, url="/list-order"){
    $.ajax({type: "get",
            url: url,
    }).done(function(data) {
        func(data.results);
        modal.modal("show");
    }).fail(function(e) {
        error(e);
    });
}
